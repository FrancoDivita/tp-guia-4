/* 3) El usuario ingresa dos strings. Mostrar en pantalla si son iguales o no, es decir, si tienen las mismas letras en las mismas posiciones.*/

#include <stdio.h>

int main()
{

char pal1[10],pal2[10];  // PALABRAS DE HASTA 10 LETRAS
  int igualdad=0;
  int numeros1 = 0, numeros2 = 0;

  printf("Ingrese una palabra: ");  //PIDE QUE INGRESE UNA PRIMER PALABRA
  scanf("%s",pal1);
  printf("Ingrese otra palabra: ");  //PIDE QUE INGRESE UNA SEGUNDA PALABRA
  scanf("%s",pal2);

  while(pal1[numeros1] != '\0')  //CUENTA LAS LETRAS DE LA PRIMER PALABRA
    {
        numeros1++;
    }
    while(pal2[numeros2] != '\0')  //CUENTA LAS LETRAS DE LA SEGUNDA PALABRA
    {
        numeros2++;
    }

    if(numeros1 != numeros2)  // SI LA CANTIDAD DE LETRAS ES DISTINTA YA TE MUSTRA EN PANTALLA QUE LAS PALABRAS NO SON LAS MISMAS
    {
        printf("Las palabras son distintas");
    }
    else     // SI LA CANTIDAD DE LETRAS ES LA MISMA VA PREGUNTANDO LETRA POR LETRA SI COINCIDEN
    {
         for (int i=0;i<=numeros1;i++){
      if(pal1[i]!=pal2[i])
        igualdad=1;
}
if(igualdad==0)   //SI SON TODAS LAS LETRAS IGUALES LA IGUALDAD VA A SEGUIR EN 0 Y TE VA A MOSTRAR EN PANTALLA QUE LAS PALABRAS SON IGUALES
{
  printf("Las palabras son iguales");
}
else      //SI ALGUNA LETRA ES DISTINTA IGUALDAD VA A CAMBIAR A 1 Y TE VA A MOSTRAR EN PANTALLA QUE LAS PALABRAS SON IDISTINTAS
{

printf("Las palabras son distintas");
}
}
    }
