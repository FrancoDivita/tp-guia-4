/* 7) El usuario ingresar� 5 nombres de personas y sus edades (n�mero entero).Luego de finalizar el ingreso, muestre en pantalla el
nombre de la persona m�s joven. El ingreso se realiza de este modo: nombre y edad de la primera persona, luego nombre y edad de la
segunda, etc... Nota: no hay que almacenar todos los nombres y todas las notas.*/

#include <stdio.h>

int main()
{
    char nombre[10], persona[10];  //PERMITO QUE NOMBRES DE 10 LETRAS
    int edad, i, menosedad = 1000;


    for(i=0; i<5; i++)
    {
        printf("\nIngrese el nombre del empleado %d:", i); //PIDO QUE INGRESE EL NOMBRE DE LAS PERSONAS
        scanf("%s", &nombre);

        printf("Ingrese la Edad del empleado %d:", i);  //PIDO QUE INGRESE LA EDAD DE LAS PERSONAS
        scanf("%d", &edad);


        if(edad < menosedad)  //COMPARA LA EDAD QUE INGRESA CON LA DE MAYOR EDAD, Y SI LA SUPERA LA GUARDA
        {
            strcpy(persona, nombre);
            menosedad = edad;
        }
    }
    printf("\nEl empleado con menos edad es %s\n", persona); //MUESTRO EN PANTALLA EL NOMBRE DE LA PERSONA MAS JOVEN
}
