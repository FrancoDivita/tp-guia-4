/* 5) El usuario ingresa una palabra. Mostrar en pantalla cu�ntas vocales min�sculas y may�sculas contiene. */

#include <stdio.h>

int main()
{
    char palabra[50];  //PERMITO PALABRAS DE HASTA 50 CARACTERES
    int i = 0;
    int Vmay = 0;
    int Vmin = 0;

    printf("Ingrese una palabra:");  //PIDO QUE INGRESE UNA PALABRA
    scanf("%s", &palabra);

    while(palabra[i] != '\0')   //PERMITE PASAR DE LETRA EN LETRA HASTA LA ULTIMA
    {
        if(palabra[i] == 'a' || palabra[i] == 'e' || palabra[i] == 'i' || palabra[i] == 'o' || palabra[i] == 'u')  //VA COMPARANDO LAS LESTRA CON LAS VOCALES MINUSCULAS Y SI ES LE SUMA VMIN LO SUMA
        {
            Vmin++;
        }

        if(palabra[i] == 'A' || palabra[i] == 'E' || palabra[i] == 'I' || palabra[i] == 'O' || palabra[i] == 'U')  //VA COMPARANDO LAS LESTRA CON LAS VOCALES MAYUSCULAS Y SI ES LE SUMA VMAX LO SUMA
        {
            Vmay++;
        }
        i++;
    }
    printf("Hay %d vocales minusculas y %d mayusculas", Vmin, Vmay); //MUSTRA EN PANTALLA CUANTAS LETRAS MINUSCULAS Y MAYUSCULAS HAY

}
