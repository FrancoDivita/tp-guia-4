/* 1) Permitir que el usuario ingrese una palabra de hasta 20 letras. Mostrar en pantalla cu�ntas letras tiene la palabra ingresada.
Por ejemplo "CASA" debe indicar 4 letras.*/

#include <stdio.h>

int main ()
{
    char palabra[21]; //DECLARO QUE TENGA HASTA 20 LETRAS
    int numeros = 0;
    int i = 0;

    printf("Ingrese una palabra:"); //PIDO QUE INGRESE UNA PALABRA
    scanf("%s", &palabra);

        while(palabra[i] != '\0')  //SIRVE PARA PASAR DE CARACTER EN CARACTER HASTA EL ULTIMO, Y CON EL NUMERO++ LOS CUENTO
        {
            numeros++;
            i++;
        }

    printf("la palabra tiene %d letras", numeros); //MUESTRA EN PANTALLA LA CANTIDAD DE CARACTERES QUE TIENE.
}
