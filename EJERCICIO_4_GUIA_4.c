/* 4) El usuario ingresa una palabra. Mostrar en pantalla cuántas letras A minúsculas contiene.*/

#include <stdio.h>

int main ()
{
    char palabra[21];  //LA PALABRA PUEDE TENER HASTA 20 LETRAS
    int suma = 0;
    int i = 0;

    printf("Ingrese una palabra:");  //PIDO QUE INGRESE UNA PALABRA
    scanf("%s", &palabra);

    while(palabra[suma] != '\0')   //SIRVE PARA PASAR DE LETRA EN LETRA HASTA LA ULTIMA COMPARANDOLAS CON UNA A
    {
        if(palabra[suma] == 'a')
        {
            i++;
        }
        suma++;    //SIRVE PARA CONTAR L CANTIDAD DE LETRAS IGUALES
    }

    printf("\nEn la palabra hay %d letras A minusculas", i);  //MUESTRO EN PANTALLA CUANTAS A MINUSCULAS IENE LA PALABRA
}
