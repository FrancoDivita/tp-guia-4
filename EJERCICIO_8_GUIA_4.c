/* 8) El usuario ingresar� nombres de personas hasta que ingrese la palabra "FIN". No sabemos cu�ntos nombres ingresar�.
Luego de finalizar el ingreso, mostrar en pantalla cu�l es el primer nombre en orden alfab�tico de todos los ingresados.*/

#include <stdio.h>

int main (){
    int i = 0;
    char nombre[30] = {0};  // PERMITE NOMBRES DE HASTA 30 LETRAS
    char nom[30] = {100};
    while(i>=0){
        printf("\nIngrese el nombre de la persona %d o FIN: ", i);  //PIDE INGRESAR NOMBRES
        scanf("%s", nombre);
        if (strcmp(nombre,"FIN")==0)  //COMPARA LOS NOMBRES QUE INGRESAN CON FIN Y SI ES, DEJA DE PODER INGRESAR NOMBRES
        {
            i = -2;
        }
        else{
            if (strcmp(nombre,nom)<0)  //COMPARA LOS COMBRES QUE ENTAN CON EL FIJO, SI ESTAN ANTES ALFABETICAMENTE TOMAN SU LUGAR
            {
                strcpy(nom,nombre);
            }
        }
        i++;
    }
    printf("\n1er Persona alfabeticamente: %s", nom);  //MUESTRA EN PANTALLA LA PRIMER PERSONA ALFABETICAMENTE
}
