/* 9) El usuario ingresar� una palabra de hasta 10 letras. Se desea mostrarla en pantalla pero encriptada seg�n el "C�digo C�sar".
Esto consiste en reemplazar cada letra con la tercera consecutiva en el abecedario. Por ejemplo, "CASA" se convierte en "FDVD".
Tener en cuenta que las �ltimas letras deben volver al inicio, por ejemplo la Y se convierte B.
Este mecanismo se utilizaba en el Imperio Romano.*/

#include <stdio.h>
#include <string.h>
int main (){
    char pal[10] = {0};  // PALABRA DE 10 LETRAS MAXIMO
    printf("Ingrese una palabra de hasta 10 letras en mayusculas: "); //PIDO QUE INGRESE UNA PALABRA
    scanf("%s", pal);
    for (int i = 0; i < strlen(pal); i++) // RECORRE LA PALABRA LETRA POR LETRA
    {   pal[i] = (pal[i] + 3);   // PERMITE QUE LA LETRA SE DESPLAZE 3 A LA IZQUIERDA

        if (pal[i] + 3 > 93)    //PERMITE QUE LA XYZ VUELVAN A INICAR EN EL ABECEDARIO
        {
            pal[i] = pal[i] - 26;
        }
    }
    printf("\nPalabra encriptada: %s", pal);  //MUESTRO EN PANTALLA LA PALABRA INCRIPTADA
}

