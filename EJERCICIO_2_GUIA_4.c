/* 2) Permitir el ingreso de una palabra y mostrarla en pantalla al rev�s. Por ejemplo, para "CASA" se debe mostrar "ASAC".*/

#include <stdio.h>

int main()
{
    char cadena[200]; //MAXIMO HASTA 200 LETRAS
    int i = 0;
    int j;
    printf("Introduce una palabra: \n");  //LE PIO QUE INTRODUZCA UNA PALABRA
    gets(cadena);
    while(cadena[i]!='\0')  //SIRVE PARA SABER LA CANTIDAD DE CARACTERES
    {
        i++;
    }
    printf("La cadena al rev\x82s es:  ");  // MUSTRA EN PANTALLA LA PALABRA AL REVES, ARRANCANDO EN LA ULTIMA LETRA Y CADA CICLO DEL FOR LE VA RESTANDO UN LUGAR Y MUSTRA LA LETRA ANTERIOR
    for (j=i-1; j>=0; j--){
        printf("%c", cadena[j]);
    }
    return 0;
}
